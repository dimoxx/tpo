public class Query {
    private String _searchQuery;

    Query(String searchQuery){
        _searchQuery = searchQuery;
    }

    public String getSearchQuery() {
        return _searchQuery;
    }
}
