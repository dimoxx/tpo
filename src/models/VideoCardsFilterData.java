public class VideoCardsFilterData {

    private String _leftPrice;
    private String _rightPrice;
    private String _keyword;

    public String getLeftPrice() {
        return _leftPrice;
    }

    public String getRightPrice() {
        return _rightPrice;
    }

    public String getKeyword() {
        return _keyword;
    }

    VideoCardsFilterData(String leftPrice, String rightPrice, String keyWord) {
        _leftPrice = leftPrice;
        _rightPrice = rightPrice;
        _keyword = keyWord;
    }
}
