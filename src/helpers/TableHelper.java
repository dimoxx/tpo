import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class TableHelper {
    public static int getRowsInTable(List<WebElement> rows) {
        return rows.size();
    }

    private static List<WebElement> getRows(TableResultPage page) {
        List<WebElement> rows = page.getResultstable().findElements(By.tagName("tr"));
        if (rows.size() < 3) {
            return new ArrayList<>();
        }
        return rows.subList(2, rows.size());
    }

    public static List<WebElement> getBasketRows(BasketPage page){
        WebElement table = page.getBasketTable();
        return table.findElements(By.tagName("tr"));
    }

    private static List<String> getSearchResultsTexts(TableResultPage page){
        return getRows(page).stream()
                .map(x -> x.findElement(By.xpath("td[1]/span/a/span")))
                .map(WebElement::getText)
                .map(String::toLowerCase)
                .collect(Collectors.toList());
    }

    private static boolean findAll(HashSet<String> first, String[] second){
        boolean result = true;
        for (String secondElem : second ) {
            result &= first.contains(secondElem);
        }
        return result;
    }

    public static List<WebElement> getPricesElements(TableResultPage page){
        return  getRows(page).stream()
                .map(x -> x.findElement(By.xpath("td[4]/span")))
                .collect(Collectors.toList());
    }

    private static List<Integer> getPricesValues(TableResultPage page) {
        return getPricesElements(page).stream()
                .map(x -> Integer.parseInt(x.getText()))
                .collect(Collectors.toList());
    }

    public static boolean isQueryTextContainsInResults(TableResultPage page, Query query){
        String[] queryWords = query.getSearchQuery().toLowerCase().split(" ");
        return getSearchResultsTexts(page).stream()
                .map(x -> new HashSet<>(Arrays.asList(x.split(" "))))
                .allMatch(y -> findAll(y, queryWords));
    }

    public static boolean isResultsPricesCorrect(TableResultPage page, VideoCardsFilterData data){
        Integer leftPrice = Integer.parseInt(data.getLeftPrice());
        Integer rightPrice = Integer.parseInt(data.getRightPrice());
        return getPricesValues(page).stream()
                .allMatch(x -> leftPrice <= x && x <= rightPrice);
    }
}
