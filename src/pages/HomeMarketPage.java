import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomeMarketPage extends BasePage {

    @FindBy(xpath = "//*[@id=\"textfield\"]")
    WebElement _searchTextField;

    @FindBy(xpath = "//*[@id=\"search_toolbar\"]/input[2]")
    WebElement _searchButton;

    HomeMarketPage(WebDriver driver){
        super(driver);
    }

    public SearchResultPage searchWithResults(Query query){
        search(query);
        return new SearchResultPage(_driver);
    }

    public EmptyResultsPage searchWithoutResults(Query query){
        search(query);
        return new EmptyResultsPage(_driver);
    }

    private void search(Query query){
        _searchTextField.click();
        _searchTextField.sendKeys(query.getSearchQuery());
        _searchButton.click();
    }
}
