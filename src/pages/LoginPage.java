import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage{

    @FindBy(xpath = "//*[@id=\"wrapcentre\"]/form/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[2]/input")
    private WebElement _loginTextField;

    @FindBy(xpath = "//*[@id=\"wrapcentre\"]/form/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/input")
    private WebElement _passwordTextField;

    @FindBy(xpath = "//*[@id=\"wrapcentre\"]/form/table/tbody/tr[3]/td/input[3]")
    private WebElement _loginButton;

    LoginPage(WebDriver driver){
        super(driver);
    }

    public AfterAuthPage authWithCorrectData(User user){
        auth(user);
        return new AfterAuthPage(_driver);
    }

    public ErrorLoginPage authWithIncorrectData(User user){
        auth(user);
        return new ErrorLoginPage(_driver);
    }

    private void auth(User user){
        setTextIntoElement(_loginTextField, user.getLogin());
        setTextIntoElement(_passwordTextField, user.getPassword());
        _loginButton.click();
    }
}
