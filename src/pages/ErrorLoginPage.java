import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
public class ErrorLoginPage extends LoginPage {

    @FindBy(xpath = "//*[@id=\"wrapcentre\"]/form/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td/span")
    private WebElement _errorTextField;

    ErrorLoginPage(WebDriver driver) {
        super(driver);
    }

    public String getErrorText(){
        return getTextFromElement(_errorTextField);
    }
}
