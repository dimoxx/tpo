import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

class BasePage {

    WebDriver _driver;

    BasePage(WebDriver driver){
        _driver = driver;
        PageFactory.initElements(_driver, this);
    }

    void setTextIntoElement(WebElement element, String text){
        element.click();
        element.sendKeys(text);
    }

    String getTextFromElement(WebElement element){
        return element.getText();
    }
}
