import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EmptyResultsPage extends BasePage {

    @FindBy(xpath = "//*[@id=\"search_goods\"]/h3")
    WebElement _emptyResults;

    EmptyResultsPage(WebDriver driver){
        super(driver);
    }

    public String getEmptyResultText(){
        return _emptyResults.getText();
    }
}
