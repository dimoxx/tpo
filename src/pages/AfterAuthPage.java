import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AfterAuthPage extends BasePage {

    @FindBy(xpath = "//*[@id=\"menubar\"]/table/tbody/tr/td[1]/a")
    private WebElement _afterLoginField;

    AfterAuthPage(WebDriver driver){
        super(driver);
    }

    public String getAfterLoginText(){
        return getTextFromElement(_afterLoginField);
    }
}
