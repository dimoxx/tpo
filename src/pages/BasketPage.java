import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

class BasketPage extends BasePage {

    private WebDriverWait _webDriverWait;

    @FindBy(xpath = "html/body/div[2]/div[1]/table[1]")
    private WebElement _basketTable;

    @FindBy(css = "button.btn-i:nth-child(2)")
    private WebElement _clearButton;

    public WebElement getBasketTable(){
        return _basketTable;
    }

    BasketPage(WebDriver driver) {
        super(driver);
        _webDriverWait = new WebDriverWait(_driver, 5, 50);
    }

    public void clear(){
        _webDriverWait.until(x -> x.findElements(By.id("shopbasket")).size() > 0);
        _driver.switchTo().frame("shopbasket");
        _clearButton.click();
        _webDriverWait.until(ExpectedConditions.alertIsPresent());
        _driver.switchTo().alert().accept();
        _webDriverWait.until(x -> TableHelper.getBasketRows(this).size() == 2);
    }
}