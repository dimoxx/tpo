import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

class VideoCardsPage extends TableResultPage {

    private WebDriverWait _webDriverWait;

    @FindBy(xpath = "//*[@id=\"l_price\"]")
    WebElement _leftpriceField;

    @FindBy(xpath = "//*[@id=\"h_price\"]")
    WebElement _rigthPriceField;

    @FindBy(xpath = "//*[@id=\"keywords\"]")
    WebElement _keywordField;

    @FindBy(xpath = "//*[@id=\"total_goods\"]")
    WebElement _searchResultsCount;

    VideoCardsPage(WebDriver driver) {
        super(driver);
        _webDriverWait = new WebDriverWait(_driver, 5, 200);
    }

    public void filterVideoCards(VideoCardsFilterData data) {
        String old = _searchResultsCount.getText();
        filterStep(_leftpriceField, data.getLeftPrice(), old);
        old = _searchResultsCount.getText();
        filterStep(_rigthPriceField, data.getRightPrice(), old);
        old = _searchResultsCount.getText();
        filterStep(_keywordField, data.getKeyword(), old);
    }

    public int getOrdersInBasket() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // _webDriverWait.until(x -> (boolean)((JavascriptExecutor)x).executeScript("return document.readyState === 'complete'"));
        List<WebElement> prices = TableHelper.getPricesElements(this).subList(0, 2);
        for (int i = 0; i < 2; ++i) {
            prices.get(i).click();
            int j = i;
            _webDriverWait.until(x -> x.findElements(By.className("price-offers-list")).size() >= (j + 1));
        }
        List<WebElement> buyTables = _driver.findElements(By.className("price-offers-list"));
        for (WebElement table : buyTables) {
            WebElement buy = table.findElement(By.xpath("tbody/tr[2]/td[6]/a"));
            buy.click();
        }
        _driver.findElement(By.xpath("//*[@id=\"BasketCellCB\"]")).click();
        _webDriverWait.until(x -> x.findElements(By.id("shopbasket")).size() > 0);
        _driver.switchTo().frame("shopbasket");
        _webDriverWait.until(x -> x.findElement(By.xpath("//*[@id=\"writeroot\"]/table[1]/tbody"))
                .findElements(By.tagName("tr")).size() >= 4);
        return _driver.findElement(By.xpath("//*[@id=\"writeroot\"]/table[1]/tbody"))
                .findElements(By.tagName("tr")).size();
    }

    private void filterStep(WebElement element, String data, String old) {
        element.click();
        element.clear();
        element.sendKeys(data);
        _webDriverWait.until(x -> !_searchResultsCount.getText().equals(old));
    }
}