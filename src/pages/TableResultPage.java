import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TableResultPage extends BasePage{

    @FindBy(xpath = "//*[@id=\"search_results\"]")
    WebElement _resultstable;

    public WebElement getResultstable() {
        return _resultstable;
    }

    TableResultPage(WebDriver driver){
        super(driver);
    }
}
