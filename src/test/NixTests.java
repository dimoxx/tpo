import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.*;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class NixTests {

    private WebDriver _driver;
    private Properties _properties;

    @BeforeTest
    public void setUp() throws IOException {
        File file = new File("userData.properties");
        InputStream fileInputStream = new FileInputStream(file);
        _properties = new Properties();
        _properties.load(fileInputStream);
        System.setProperty("webdriver.chrome.driver", "/home/dmitry/chromedriver");
        _driver = new ChromeDriver();
        _driver.manage().window().maximize();
        _driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Test(priority = 2)
    public void correctAuth() {
        _driver.get("http://www.nix.ru/abc3/ucp.php?mode=login");
        LoginPage loginPage = new LoginPage(_driver);
        User user = new User(_properties.getProperty("login"), _properties.getProperty("password"));
        AfterAuthPage homeForumPage = loginPage.authWithCorrectData(user);
        String afterAuthText = homeForumPage.getAfterLoginText();
        assertEquals(afterAuthText, Constants.ETALON_LOGIN_TEXT);
    }

    @Test(priority = 0)
    public void authWithIncorrectLogin() {
        _driver.get("http://www.nix.ru/abc3/ucp.php?mode=login");
        LoginPage loginPage = new LoginPage(_driver);
        User user = new User("kdggjg", _properties.getProperty("password"));
        ErrorLoginPage errorLoginPage = loginPage.authWithIncorrectData(user);
        String errorText = errorLoginPage.getErrorText();
        assertEquals(errorText, Constants.ETALON_ERROR_LOGIN_TEXT);
    }

    @Test(priority = 1)
    public void authWithIncorrectPassword() {
        _driver.get("http://www.nix.ru/abc3/ucp.php?mode=login");
        LoginPage loginPage = new LoginPage(_driver);
        User user = new User(_properties.getProperty("login"), "w44i67jk");
        ErrorLoginPage errorLoginPage = loginPage.authWithIncorrectData(user);
        String errorText = errorLoginPage.getErrorText();
        assertEquals(errorText, Constants.ETALON_ERROR_PASSWORD_TEXT);
    }

    @Test(priority = 4)
    public void searchWithResults() {
        _driver.get("http://ul.nix.ru");
        Query query = new Query("GigaByte GTX 1060");
        HomeMarketPage homeMarketPage = new HomeMarketPage(_driver);
        SearchResultPage searchResultPage = homeMarketPage.searchWithResults(query);
        boolean isCorrectResult = TableHelper.isQueryTextContainsInResults(searchResultPage, query);
        assertTrue(isCorrectResult);
    }

    @Test(priority = 3)
    public void searchWithoutResults() {
        _driver.get("http://ul.nix.ru");
        Query query = new Query("пыпыып");
        HomeMarketPage homeMarketPage = new HomeMarketPage(_driver);
        EmptyResultsPage emptyResultsPage = homeMarketPage.searchWithoutResults(query);
        String resultText = emptyResultsPage.getEmptyResultText();
        assertEquals(resultText, Constants.ETALON_EMPTY_RESULTS_TEXT);
    }

    @Test(priority = 7)
    public void videoCardsFilterTest() {
        _driver.get("http://ul.nix.ru/price.html?section=video_cards_all");
        VideoCardsFilterData data = new VideoCardsFilterData("15000", "20000", "gtx 1060");
        Query query = new Query(data.getKeyword());
        VideoCardsPage videoCardsPage = new VideoCardsPage(_driver);
        videoCardsPage.filterVideoCards(data);
        boolean isCorrectResult = TableHelper.isQueryTextContainsInResults(videoCardsPage, query) &&
                TableHelper.isResultsPricesCorrect(videoCardsPage, data);
        assertTrue(isCorrectResult);
    }

    @Test(priority = 5)
    public void basketTest(){
        _driver.get("http://ul.nix.ru/price.html?section=video_cards_all");
        VideoCardsPage videoCardsPage = new VideoCardsPage(_driver);
        int orders = videoCardsPage.getOrdersInBasket() - 2;
        assertEquals(orders, 2);
    }

    @Test(priority = 6)
    public void clearBasketTest(){
        _driver.get("http://ul.nix.ru/price/virtualshop_cb.html");
        BasketPage basketPage = new BasketPage(_driver);
        basketPage.clear();
        int orders = TableHelper.getBasketRows(basketPage).size() - 2;
        assertEquals(orders, 0);
    }

    @AfterTest
    public void quit() {
        _driver.quit();
    }
}